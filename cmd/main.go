package main

import (
	"cron/conf"
	"cron/service"
	"fmt"
	"github.com/pkg/errors"
	"github.com/robfig/cron/v3"
	"os"
)

var cfg struct {
	configFile string
}

func main() {

	if len(os.Args) > 1 {
		cfg.configFile = os.Args[1]
	}
	// init config
	InitStaticConfig()

	// init service
	InitCronService()

}

func InitStaticConfig() {
	// load global config
	if _, err := conf.LoadBrainGlobalConfig(cfg.configFile); err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "Error load config file"))
		os.Exit(2)
	}
}

func InitCronService() {

	c := cron.New()

	c.AddFunc(conf.GetGlobalConfig().HttpRetryConf.ExecuteFrequency, service.Retry)
	c.AddFunc(conf.GetGlobalConfig().HttpRetryConf.RemoveFileFrequency, service.Remove)

	c.Start()
	select {}

}
