package conf

import (
	"github.com/pkg/errors"
	"gopkg.in/yaml.v2"
	"io/ioutil"
)

var (
	cronGlobalConfig *Config
)

type Config struct {
	HttpRetryConf HttpRetry `yaml:"http_retry"`
}

type HttpRetry struct {
	ExecuteFrequency    string `yaml:"execute_frequency"`
	RemoveFileFrequency string `yaml:"remove_file_frequency"`
	ProducerPath        string `yaml:"producer_path"`
	ConsumerPath        string `yaml:"consumer_path"`
	ResultPath          string `yaml:"result_path"`
	DelayMinute         int64  `yaml:"delay_minute"`
}

// Load parses the YAML input s into a Config.
func Load(s string) (*Config, error) {
	cfg := &Config{}
	err := yaml.UnmarshalStrict([]byte(s), cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

// LoadFile parses the given YAML file into a Config.
func LoadBrainGlobalConfig(filename string) (*Config, error) {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	cfg, err := Load(string(content))
	if err != nil {
		return nil, errors.Wrapf(err, "parsing YAML file %s", filename)
	}

	err = validate(cfg)
	if err != nil {
		return nil, err
	}

	cronGlobalConfig = cfg
	return cfg, nil
}

func validate(config *Config) error {
	if config == nil {
		return nil
	}
	return nil
}

func GetGlobalConfig() *Config {
	return cronGlobalConfig
}
