package service

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestConvertStrToInt(t *testing.T) {

	rs1, err1 := ConvertStrToInt("1234")

	assert.Equal(t, 1234, rs1)
	assert.Equal(t, nil, err1)

	rs2, err2 := ConvertStrToInt("123d")
	assert.Equal(t, 0, rs2)
	assert.NotEqual(t, nil, err2)

}
func TestInArray(t *testing.T) {

	tests := []struct {
		strItem  string
		strArray []string

		intItem  int
		intArray []int

		int64Item  int64
		int64Array []int64

		expectStr   bool
		expectInt   bool
		expectInt64 bool
	}{
		{"a", []string{"a", "b"}, 100, []int{1, 100, 1222}, 2022, []int64{2019, 2020, 2021, 2022}, true, true, true},
	}

	for _, tc := range tests {
		assert.Equal(t, tc.expectStr, InArray(tc.strItem, tc.strArray))
		assert.Equal(t, tc.expectInt, InArray(tc.intItem, tc.intArray))
		assert.Equal(t, tc.expectInt64, InArray(tc.int64Item, tc.int64Array))
	}

	assert.Equal(t, false, InArray("c", []float64{100, 1222}))
	assert.Equal(t, false, InArray(6, []float64{1, 100, 1222}))
	assert.Equal(t, false, InArray(int64(8), []float64{2019, 2020, 2021, 2022}))

	assert.Equal(t, false, InArray(3.14, []float64{3.14, 2.71828}))
}


func TestNonce(t *testing.T) {
	for i := 0; i < 100; i++ {
		assert.NotEqual(t, 0, Nonce())
	}
}