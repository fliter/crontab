package service

import (
	"bufio"
	"bytes"
	"cron/conf"
	"cron/model"
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

func initPath(path string) {
	_, err := os.Stat(path)
	if err != nil && errors.Is(err, os.ErrNotExist) {
		// 创建多级目录
		os.MkdirAll(path, os.ModePerm)
	}
}

func Retry() {

	initPath(conf.GetGlobalConfig().HttpRetryConf.ConsumerPath)
	// 前x分钟
	delayMinute := conf.GetGlobalConfig().HttpRetryConf.DelayMinute
	lastMin := time.Now().Add(-time.Duration(delayMinute) * time.Minute).Format("2006-01-02Day 15:04Min")

	producerFile := conf.GetGlobalConfig().HttpRetryConf.ProducerPath + lastMin

	//判断该文件是否存在
	_, err := os.Stat(producerFile)
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "没有产生新的待消费信息"))
		return
	}
	consumerFile := conf.GetGlobalConfig().HttpRetryConf.ConsumerPath + lastMin

	// 移动文件到一个新目录中
	err = os.Rename(producerFile, consumerFile)

	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "移动文件失败"))
		return
	}
	b, e := ioutil.ReadFile(consumerFile)
	if e != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "读取文件失败"))
		return
	}
	reqSli := strings.Split(string(b), "\n")

	for _, item := range reqSli {
		if len(item) == 0 {
			continue
		}

		reqItem := new(model.RetryMeta)
		err = json.Unmarshal([]byte(item), reqItem)
		if err != nil {
			fmt.Fprintln(os.Stderr, errors.Wrapf(err, "json.Unmarshal err"))
			return
		}
		err = retryFunc(reqItem, lastMin)
		if err != nil {
			fmt.Fprintln(os.Stderr, errors.Wrapf(err, "retryFunc err"))
			return
		}
	}

	// 删除该文件 (不要立即删，可每天定时清理)
	//os.RemoveAll(consumerFile)
}

func retryFunc(reqItem *model.RetryMeta, lastMin string) (err error) {
	client := &http.Client{}
	buffer := bytes.NewBuffer(reqItem.Req)
	req, err := http.NewRequest(reqItem.Method, reqItem.Url, buffer)

	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "http.NewRequest err"))
		return
	}
	for k, v := range reqItem.Headers {
		req.Header.Set(k, v)
	}

	res, err := client.Do(req)
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "client.Do err"))
		return
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "ioutil.ReadAll err"))
		return
	}

	// 将返回的结果落盘
	err = saveResult(string(body), reqItem.Uuid, lastMin)

	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "saveResult err"))
	}
	return
}

func saveResult(rs, uuid, lastMin string) (err error) {
	path := conf.GetGlobalConfig().HttpRetryConf.ResultPath
	initPath(path)

	file, err := os.OpenFile(path+lastMin, os.O_WRONLY|os.O_APPEND, 0666)
	defer file.Close()

	if err != nil && os.IsNotExist(err) {
		file, _ = os.Create(path + lastMin)
	}

	retryResult := new(model.RetryResult)
	retryResult.Rs = rs
	retryResult.Uuid = uuid

	//组装并写入文件
	rsStr, err := json.Marshal(retryResult)

	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "saveResult json.Marshal err"))
		return
	}

	write := bufio.NewWriter(file)
	write.WriteString(string(rsStr) + "\n")
	write.Flush()
	return
}

func Remove() {

	ConsumerPath := conf.GetGlobalConfig().HttpRetryConf.ConsumerPath
	lastDay := time.Now().Add(-time.Hour).Format("2006-01-02Day")

	path := ConsumerPath + lastDay + "*"

	files, err := filepath.Glob(path)
	if err != nil {
		fmt.Fprintln(os.Stderr, errors.Wrapf(err, "Remove filepath.Glob err"))
		return
	}
	for _, f := range files {
		if err := os.Remove(f); err != nil {
			fmt.Fprintln(os.Stderr, errors.Wrapf(err, "Remove filepath.Glob err"))
		}
	}

}

func ConvertStrToInt(str string) (int, error) {
	rs, err := strconv.Atoi(str)
	if err != nil {
		return 0, err
	}
	return rs, nil
}

func InArray(v interface{}, array interface{}) bool {
	switch v.(type) {
	case int:
		items, ok := array.([]int)
		if !ok {
			return false
		}
		for _, item := range items {
			if v == item {
				return true
			}
		}
	case int64:
		items, ok := array.([]int64)
		if !ok {
			return false
		}
		for _, item := range items {
			if v == item {
				return true
			}
		}
	case string:
		items, ok := array.([]string)
		if !ok {
			return false
		}
		for _, item := range items {
			if v == item {
				return true
			}
		}
	}

	return false
}

func GenerateToken(cid int64) string {
	s := fmt.Sprintf("%d", cid)
	h := sha1.New()
	h.Write([]byte(s))
	bs := h.Sum(nil)
	return fmt.Sprintf("%x", bs)
}

// Nonce generates a random number
func Nonce() int64 {
	return rand.New(rand.NewSource(time.Now().UnixNano())).Int63n(10000000000)
}
