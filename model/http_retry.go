package model

import "encoding/json"

type RetryMeta struct {
	Method string `json:"method"`
	Url    string `json:"url"`
	Key    string `json:"key"`
	Uuid   string `json:"uuid"`
	//Req     interface{}       `json:"req"`
	Req     json.RawMessage   `json:"req"`
	Headers map[string]string `json:"headers"`
}

type RetryResult struct {
	Rs   string `json:"result"`
	Uuid string `json:"uuid"`
}
